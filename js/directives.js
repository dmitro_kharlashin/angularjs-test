var directives = angular.module("directives", []);

directives.directive("mainTabs", function(){
    return{
        restrict: "E",
        link: function(scope, element, attributes){
            element.addClass('main-tabs');
        },
        templateUrl: "partials/main-tabs.html"
    }
});

directives.directive("developTab", function(){
    return{
        restrict: "E",
        templateUrl: "partials/develop-tab.html"
    }
});

directives.directive("teamTab", function(){
    return{
        restrict: "E",
        templateUrl: "partials/team-tab.html"
    }
});

directives.directive("addTeam", function(){
    return{
        restrict: "E",
        templateUrl: "partials/add-team.html"
    }
});

directives.directive("leftColumn", function(){
    return{
        restrict: "E",
        templateUrl: "partials/left-column.html",
        controller: function(){

            this.setTeam = function(activeTeam){
                scope.activeTeam = activeTeam;
            };

            this.show = function(){
                return sessionStorage.tab != 'team-tab';
            }
        },
        controllerAs: "project"
    }
});