(function() {

    var app = angular.module("developStore", ["ngRoute", "ui.bootstrap", "controllers", "directives"]);

    angular.module('ui.bootstrap').controller('AccordionDemoCtrl', function ($scope) {
        $scope.oneAtATime = true;
    });

    app.config(function($routeProvider){
        $routeProvider.
            when('/', {
                templateUrl: "partials/content.html",
                controller: "TabsController as tab"
            }).
            when('/:tabName', {
                templateUrl: "partials/content.html",
                controller: 'TabsController as tab'
            }).
            otherwise({
                redirectTo: '/'
            });
    });

})();