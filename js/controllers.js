var scope = [];

var controllers = angular.module("controllers", []);

controllers.controller("StoreController", ["$scope", "$http", function($scope, $http){
    var store = this;
    store.developers = [];

    $http.get("storage/developers.php").success(function(data){
        store.developers = data;
    });
    //db.transaction(function(tx) {
    //    tx.executeSql("SELECT COUNT(*) FROM Developers", [], function (result) {
    //        angular.forEach(store.developers, function(key, value){
    //            store.developers[value].id = parseInt(key.$$hashKey.split(':')[1]);
    //        })
    //    }, function (tx, error) {
    //        tx.executeSql("CREATE TABLE Developers (id INT UNIQUE, name TEXT, speciality TEXT, age INT, level TEXT)", [], null, null);
    //        angular.forEach(store.developers, function(key, value){
    //            store.developers[value].id = parseInt(key.$$hashKey.split(':')[1]);
    //            tx.executeSql("INSERT INTO Developers (id, name, speciality, age, level) VALUES(?, ?, ?, ?, ?)", [store.developers[value].id, store.developers[value].name, store.developers[value].speciality, store.developers[value].age, store.developers[value].level], null, null);
    //        });
    //    })
    //});

    store.search = '';

    store.isSearching = function () {
        return store.search.length;
    };

    var limit = 5;
    store.limit = limit;
    store.page = 1;

    store.setPage = function(n){
        if (n >= 1 && n <= store.countPages()) {
            store.page = n;
        }
    };

    store.range = function() {
        var developers = [];

        if(store.isSearching()){
            store.limit = store.developers.length;
            developers = store.developers;
        }else{
            store.limit = (store.developers.length < 5) ? store.developers.length : 5;
            var rangeSize = store.limit;
            var start = store.limit*(store.page - 1);
            var offset = (start + rangeSize > store.developers.length) ? store.developers.length : start + rangeSize;

            for (var i = start; i < offset; i++) {
                developers.push(store.developers[i]);
            }
        }

        return developers;
    };

    store.countPages = function(){
        return Math.ceil(store.developers.length/store.limit);
    };

    store.prevPageDisabled = function() {
        return store.page === 1 ? true : false;
    };

    store.nextPageDisabled = function() {
        return store.page === store.countPages() ? true : false;
    };

}]);

controllers.controller("TeamsController", ["$scope", "$http", function($scope, $http){
    var teams = this;
    teams.projects = [];
    var limit = 2;
    teams.limit = limit;
    teams.page = 0;

        //if (angular.fromJson(localStorage.getItem('teams'))){
        //    teams.projects = angular.fromJson(localStorage.getItem('teams'));
        //}else{
    $http.get("storage/teams.php").success(function(data){
            teams.projects = data;
    });
        //    localStorage.setItem('teams', angular.toJson(teams.projects));
        //}

    teams.loop = function(page){
        var searchedTeams = [];
        teams.limit = limit*(page + 1);
        var endSearch = (teams.projects.length < (teams.limit + limit - 1)) ? teams.projects.length : (teams.limit + limit - 1);

        for (var i = 0; i < endSearch; i++) {
            searchedTeams.push(teams.projects[i]);
        }

        return searchedTeams;
    };

    teams.setPage = function(n) {
        teams.page = n;
    };

    teams.countPages = function () {
        return Math.ceil(teams.projects.length/limit) - 1;
    };

    teams.nextPageDisabled = function(){
        return teams.page === teams.countPages();
    };

    //db.transaction(function(tx) {
    //    tx.executeSql("CREATE TABLE Teams IF NOT EXISTS (id INT UNIQUE, name TEXT)", [], null, null);
    //    angular.forEach(teams.projects, function(key, value){
    //        teams.projects[value].id = parseInt(key.$$hashKey.split(':')[1]);
    //        tx.executeSql("INSERT INTO Teams (id, name) VALUES(?, ?)", [teams.projects[value].id, teams.projects[value].name], null, null);
    //    });
    //});
    //db.transaction(function(tx){
    //var loadTeam = function(projects){
    //    teams.projects = [];
    //    angular.forEach(projects, function(key, value){
    //        teams.projects.push(projects[value]);
    //    });
    //    console.log(teams.projects);
    //};
    //var projects = [];
    //tx.executeSql("SELECT * FROM Teams", [], function(t, rows) {
    //    teams.projects = [];
    //    $scope.projects = [];
    //    for(var i = 0; i < rows.rows.length; i++) {
    //        $scope.projects.push(rows.rows.item(i));
    //    }
    //    console.log($scope);
    //});
    //});

    //db.transaction(function (tx) {
    //    tx.executeSql("SELECT COUNT(*) FROM Developers_Teams", [], function (result) { alert('DB table is isset') }, function (tx, error) {
    //        tx.executeSql("CREATE TABLE Developers_Teams (developers_id INT, teams_id INT)", [], null, null);
    //    });
    //});

    teams.isEmpty = function(team){
        return !team.developers.length;
    };

    teams.addTeam = function(){
        var newTeam = this.project;

        $http.get('../app/add_team.php?name='+newTeam.name).success(function(data){
            //var now = new Date();
            //console.log(now.toJSON());
            //self.project.created = now.toJSON();

            newTeam.created = data.created;
            newTeam.id = data.id;
            newTeam.developers = [];
            teams.projects.unshift(newTeam);

            //localStorage.setItem('teams', angular.toJson(self.projects));
        });

        //db.transaction(function(tx) {
        //    self.project.id = parseInt(self.project.$$hashKey.split(':')[1]);
        //    tx.executeSql("INSERT INTO Teams (id, name) values(?, ?)", [self.project.id, self.project.name], null, null);
        //});

        this.project = [];
    };

    teams.delTeam = function(team, e){
        e.preventDefault();
        e.stopPropagation();

        $http.get('../app/delete_team.php?id='+team.id).success(function(data){
            var i = teams.projects.indexOf(team);
            teams.projects.splice(i, 1);

            //localStorage.setItem('teams', angular.toJson(teams.projects));
        });

    };

    teams.addDeveloper = function(developer){

        if(scope.activeTeam /*&& angular.element($$('div .collapse.in')).length*/ && document.getElementsByClassName('collapse in').length) {

            var i = teams.projects.indexOf(scope.activeTeam);

            var developerIsSaved = teams.projects[i].developers.filter(function (entry) {
                return entry.id === developer.id;
            });
            if(!developerIsSaved.length){

                $http.get('../app/add_developer.php?teams_id='+teams.projects[i].id+'&developers_id='+developer.id).success(function(data){
                    teams.projects[i].developers.push(developer);

                    //localStorage.setItem('teams', angular.toJson(teams.projects));
                });

            }else{
                alert(developer.name + ' is already selected to current team( ' + teams.projects[i].name + ' ).');
            }
            //db.transaction(function(tx) {
            //    tx.executeSql("INSERT INTO Developers_Teams (developers_id, teams_id) values(?, ?)", [developer.id, sessionStorage.activeTeamId], null, null);
            //});
        }else{
            alert("You don't choose a team");
        }
    };

    teams.delDeveloper = function(developer, team){

        $http.get('../app/delete_developer.php?teams_id='+team.id+'&developers_id='+developer.id).success(function (data) {
            var i = team.developers.indexOf(developer);
            team.developers.splice(i, 1);

            //localStorage.setItem('teams', angular.toJson(teams.projects));
        });
    }
}]);

controllers.controller("TabsController", ['$scope', '$routeParams', function($scope, $routeParams){
    var tab = this;

    if(($routeParams.tabName)|| (sessionStorage.tab)){
        tab.name = $routeParams.tabName ? $routeParams.tabName : sessionStorage.tab;
    }else{
        tab.name = 'develop-tab';
    }

    tab.setTab = function(activeTab) {
        tab.name = activeTab;
        sessionStorage.tab = activeTab;
    };
    tab.setTab(tab.name);

    tab.isSet = function(checkTab) {
        return tab.name === checkTab;
    };

}]);
