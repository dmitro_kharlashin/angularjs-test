<?php

$teams = [];
include("../app/db.php");
$result = mysql_query("select * from teams ORDER BY created DESC", $db);

if($result){
  while ($myrow = mysql_fetch_assoc($result)){
    $teams[] = $myrow;
  }

  foreach ($teams as $key => $value) {
    $teams[$key]['developers'] = [];
    $result = mysql_query("SELECT d.id, d.name, d.speciality, d.age, d.level
FROM teams_developers AS td
INNER JOIN developers AS d ON td.teams_id =".$teams[$key]['id']." AND d.id = td.developers_id", $db);

    while ($myrow = mysql_fetch_assoc( $result)){
      $teams[$key]['developers'][] = $myrow;
    }
  }

  $teams = json_encode($teams);
} else {
  $file = file_get_contents('teams.json');
  $teams = !empty($file) ? $file : json_encode([]);
}

echo $teams;